//
// Created by Robbert-Jan de Jager on 2-2-19.
//

#include <engine/GLWindow.h>
#include <engine/engine.h>
#include <cmath>

using namespace rj;

constexpr int BLOCK_WIDTH = 5;
constexpr int BLOCK_HEIGHT = 5;
constexpr int GRID_WIDTH = 12;
constexpr int GRID_HEIGHT = 20;
constexpr Color BACKGROUND_COLOR = color::SILVER;
constexpr Color HIGHLIGHT_COLOR(color::WHITE.rgba);
constexpr Color TETRIMINO_COLOR[] = {
		color::AQUA,
		color::BLUE,
		color::LIME,
		color::YELLOW,
		color::OLIVE,
		color::PURPLE,
		color::RED,
};

static constexpr char Block[BLOCK_HEIGHT][BLOCK_WIDTH] = {
		{1,1,1,1,0},
		{1,1,2,1,0},
		{1,2,2,1,0},
		{1,1,1,1,0},
		{0,0,0,0,0}
};

static constexpr char Tetriminos[][4][4][4] = {
	{
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{1, 1, 1, 1},
			{0, 0, 0, 0}
		},
        {
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 1, 0, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{1, 1, 1, 1},
			{0, 0, 0, 0}
		},
		{
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 1, 0, 0},
			{0, 1, 0, 0}
		}
	},
	{
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 2, 0, 0},
			{0, 2, 2, 2}
		},
		{
			{0, 0, 0, 0},
			{0, 2, 2, 0},
			{0, 2, 0, 0},
			{0, 2, 0, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 2, 2, 2},
			{0, 0, 0, 2}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 2, 0},
			{0, 0, 2, 0},
			{0, 2, 2, 0}
		},
	},
	{
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 3, 0},
			{3, 3, 3, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 3, 0, 0},
			{0, 3, 0, 0},
			{0, 3, 3, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{3, 3, 3, 0},
			{3, 0, 0, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 3, 3, 0},
			{0, 0, 3, 0},
			{0, 0, 3, 0}
		},
	},
	{
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 4, 4, 0},
			{0, 4, 4, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 4, 4, 0},
			{0, 4, 4, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 4, 4, 0},
			{0, 4, 4, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 4, 4, 0},
			{0, 4, 4, 0}
		},
	},
	{
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 5, 5, 0},
			{5, 5, 0, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 5, 0, 0},
			{0, 5, 5, 0},
			{0, 0, 5, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 5, 5, 0},
			{5, 5, 0, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 5, 0, 0},
			{0, 5, 5, 0},
			{0, 0, 5, 0}
		},
	},
	{
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 6, 0, 0},
			{6, 6, 6, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 6, 0, 0},
			{0, 6, 6, 0},
			{0, 6, 0, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{6, 6, 6, 0},
			{0, 6, 0, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 6, 0, 0},
			{6, 6, 0, 0},
			{0, 6, 0, 0}
		},
	},
	{
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{7, 7, 0, 0},
			{0, 7, 7, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 7, 0},
			{0, 7, 7, 0},
			{0, 7, 0, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{7, 7, 0, 0},
			{0, 7, 7, 0}
		},
		{
			{0, 0, 0, 0},
			{0, 0, 7, 0},
			{0, 7, 7, 0},
			{0, 7, 0, 0}
		},
	}
};

class Tetris : public rj::GLWindow
{
	char _grid[GRID_HEIGHT][GRID_WIDTH];
	int _activeX;
	int _activeY;
	int _activeShape;
	int _activeRotation;
	float _timeSinceStep;
	int _score;
	int _nextShape;

public:
	Tetris(std::shared_ptr<Engine> engine) : rj::GLWindow(std::move(engine), 5*(GRID_WIDTH+5), 5*GRID_HEIGHT, 680, 800), _grid{}, _activeX((GRID_WIDTH-4)/2), _activeY(0),
		_activeShape(0), _activeRotation(0), _timeSinceStep(0), _score(0), _nextShape(0)
	{
		SetName("Tetris");
		_nextShape = rand() % 7;
	}
	~Tetris() override {

	}

private:
	void DrawBlock(int x, int y, Color blockColor) {
		for(int yi = 0; yi < BLOCK_HEIGHT; yi++) {
			for(int xi = 0; xi < BLOCK_WIDTH; xi++) {
				char val = Block[yi][xi];
				auto color = val == 0 ? BACKGROUND_COLOR : (val == 2 ? HIGHLIGHT_COLOR : blockColor);
				SetPixel(x+xi, y+yi, color);
			}
		}
	}

	void DrawGrid() {
		for(int y = 0; y < GRID_HEIGHT; y++) {
			for(int x = 0; x < GRID_WIDTH; x++) {
				auto block = _grid[y][x];
				if(block > 0) {
					DrawBlock(x*BLOCK_WIDTH, y*BLOCK_HEIGHT, TETRIMINO_COLOR[block-1]);
				} else {
					DrawFilledRect(Point2i(x*BLOCK_WIDTH, y*BLOCK_HEIGHT), BLOCK_WIDTH, BLOCK_HEIGHT, BACKGROUND_COLOR);
				}
			}
		}
	}

	char GetTetriminoBlock(int x, int y, int rotation, int shape) {
		return Tetriminos[shape][rotation][y][x];
	}

	void DrawActiveTetrimino() {
		for(int y = _activeY; y < _activeY + 4 && y < GRID_HEIGHT; y++) {
			for(int x = _activeX; x < _activeX + 4 && x < GRID_WIDTH; x++) {
				char val = GetTetriminoBlock(x - _activeX, y - _activeY, _activeRotation, _activeShape);
				if(val > 0) {
					DrawBlock(x * BLOCK_WIDTH, y * BLOCK_HEIGHT, TETRIMINO_COLOR[val - 1]);
				}
			}
		}
	}

	void DrawNextTetrimino() {
		for(int y = 0; y < 4; y++) {
			for(int x = 0; x < 4; x++) {
				char val = GetTetriminoBlock(x, y, 0, _nextShape);
				if(val > 0) {
					DrawBlock((GRID_WIDTH + 1 + x) * BLOCK_WIDTH, y * BLOCK_HEIGHT, TETRIMINO_COLOR[val - 1]);
				} else {
					DrawFilledRect(Point2i((GRID_WIDTH + 1 + x) * BLOCK_WIDTH, y * BLOCK_HEIGHT),
							BLOCK_WIDTH, BLOCK_HEIGHT, BACKGROUND_COLOR);
				}
			}
		}
	}

	bool IsValidPosition(int x, int y, int rotation, int shape) {
		for(int yi = 0; yi < 4; yi++) {
			for(int xi = 0; xi < 4; xi++) {
				bool isBlock = GetTetriminoBlock(xi, yi, rotation, shape) > 0;
				if(!isBlock) continue;
				int px = xi+x;
				int py = yi+y;
				if(px < 0 || px >= GRID_WIDTH || py < 0 || py >= GRID_HEIGHT || _grid[py][px] > 0) return false;
			}
		}
		return true;
	}

	void PlaceTetrimino() {
		for(int yi = 0; yi < 4; yi++) {
			for(int xi = 0; xi < 4; xi++) {
				char block = GetTetriminoBlock(xi, yi, _activeRotation, _activeShape);
				if(block == 0) continue;
				int px = xi+_activeX;
				int py = yi+_activeY;
				_grid[py][px] = block;
			}
		}
	}

	bool CreateNewTetrimino() {
		int newX = (GRID_WIDTH - 4) / 2;
		int newY = 0;
		if(!IsValidPosition(newX, newY, 0, _nextShape)) return false;
		_activeY = newY;
		_activeX = newX;
		_activeRotation = 0;
		_activeShape = _nextShape;
		_nextShape = rand() % 7;
		return true;
	}

	float GetCurrentStepTime() {
		return KeyBoard().IsPressed(Key::Down) ? 0.05f : 0.5f;
	}

	void RemoveFilledLines() {
		int multiplier = 1;
		for(int y = GRID_HEIGHT - 1; y >= 0; y--) {
			bool lineFilled = true;
			for(int x = 0; x < GRID_WIDTH; x++) {
				if(_grid[y][x] == 0) {
					lineFilled = false;
					break;
				}
			}
			if(lineFilled) {
				_score += 250 * multiplier++;
				for(int yi = y; yi > 0; yi--) {
					for(int x = 0; x < GRID_WIDTH; x++) {
						_grid[yi][x] = yi == 0 ? 0 : _grid[yi - 1][x];
					}
				}
				y++;
			}
		}
		printf("Your current score is: %d\n", _score);
	}

	void OnRender(float elapsedTime) override {
		if(KeyBoard().IsPressing(Key::F1)) {
			_activeShape = (_activeShape + 1) % 7;
		}
		if(KeyBoard().IsPressing(Key::F2)) {
			_activeY = 0;
			_activeX = (GRID_WIDTH - 4) / 2;
		}
		if(KeyBoard().IsPressing(Key::Up)) {
			int newRotation = (_activeRotation + 1) % 4;
			if(IsValidPosition(_activeX, _activeY, newRotation, _activeShape)) {
				_activeRotation = newRotation;
			}
		}
		if(KeyBoard().IsPressing(Key::Left)) {
			int newX = _activeX - 1;
			if(IsValidPosition(newX, _activeY, _activeRotation, _activeShape)) {
				_activeX = newX;
			}
		}
		if(KeyBoard().IsPressing(Key::Right)) {
			int newX = _activeX + 1;
			if(IsValidPosition(newX, _activeY, _activeRotation, _activeShape)) {
				_activeX = newX;
			}
		}
		_timeSinceStep += elapsedTime;
		float stepTime = GetCurrentStepTime();
		if(_timeSinceStep > stepTime) {
			int newY = _activeY + 1;
			if(IsValidPosition(_activeX, newY, _activeRotation, _activeShape)) {
				_activeY = newY;
			} else {
				PlaceTetrimino();
				CreateNewTetrimino();
				RemoveFilledLines();
			}
			_timeSinceStep = 0;
		}


		DrawGrid();
		DrawActiveTetrimino();
		DrawNextTetrimino();
	}


};

int main() {
	try {
		std::shared_ptr<Engine> engine = std::make_shared<Engine>();

		Tetris game(engine);

		engine->run();
	} catch(std::exception &ex) {
		fprintf(stderr, "Uncaught exception:\n%s\n", ex.what());
		abort();
	}

	return 0;
}
