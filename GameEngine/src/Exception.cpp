//
// Created by Robbert-Jan de Jager on 15-9-18.
//

#include <engine/Exception.h>
#include <cstdarg>
#include <sstream>
#include <iomanip>
#include <cstring>
#ifdef __unix__
#include <execinfo.h>
#elif WIN32
#include <Windows.h>
#include <DbgHelp.h>
#endif

using namespace rj;

bool symbolsInitialized = false;

Exception::Exception(const char *exceptionName, const char *format, ...) : runtime_error(""), m_what(nullptr) {
    va_list args;
    int msgSize;
    char *msg;
    std::stringstream ss;

    va_start(args, format);
    msgSize = vsnprintf(nullptr, 0, format, args);
    va_end(args);
    msg = new char[msgSize + 1];
    va_start(args, format);
    vsprintf(msg, format, args);
    va_end(args);

	ss << "Exception " << exceptionName << " raised: " << msg << std::endl;
	delete[] msg;

#ifdef __unix__
	void* backtraceAddr[64];
	char** backtraceSymbols;
	int numBacktrace;
	numBacktrace = backtrace(backtraceAddr, 64);
	backtraceSymbols = backtrace_symbols(backtraceAddr, numBacktrace);
	ss << "Backtrace: " << std::endl;
	for (int i = 0; i < numBacktrace; i++) {
		ss << backtraceSymbols[i] << std::endl;
	}
#elif WIN32
	void* backtraceAddr[64];
	int numBackTrace = CaptureStackBackTrace(1, 62, backtraceAddr, nullptr);
	ss << "Backtrace: " << std::endl;
	HANDLE currentProcess = ::GetCurrentProcess();
	if(!symbolsInitialized)
	{
		SymInitialize(currentProcess, nullptr, true);
	}
	for (int i = 0; i < numBackTrace; i++) {
		SYMBOL_INFO si;
		if(SymFromAddr(currentProcess, reinterpret_cast<DWORD64>(backtraceAddr[i]), 0, &si))
		{
			ss << "[0x" << backtraceAddr[i] << "] - " << si.Name << std::endl;
		} else
		{
			ss << "[0x" << backtraceAddr[i] << "] - Unresolved address" << std::endl;	
		}
	}
#endif

    std::string what_str = ss.str();
    auto *what = new char[what_str.length()+1];

    ::strcpy(what, what_str.c_str());
    m_what = what;
}

Exception::Exception(const char *exceptionName, const char *format, va_list args) : runtime_error(""), m_what(nullptr) {
    SetWhat(exceptionName, format, args);
}

Exception::~Exception() {
    if(m_what != nullptr) {
        delete[] m_what;
        m_what = nullptr;
    }
}

const char *Exception::what() const noexcept {
    return m_what;
}

Exception::Exception() : std::runtime_error(""), m_what(nullptr) {

}

void Exception::SetWhat(const char *exceptionName, const char *format, va_list args) {
    va_list args_count;
    int msgSize;
    char *msg;
    std::stringstream ss;

    va_copy(args_count, args);
    msgSize = vsnprintf(nullptr, 0, format, args_count);
    va_end(args_count);
    msg = new char[msgSize + 1];
    vsprintf(msg, format, args);

    ss << "Exception " << exceptionName << " raised: " << msg << std::endl;
	delete[] msg;

#ifdef __unix__
	void* backtraceAddr[64];
	char** backtraceSymbols;
	int numBacktrace;
	numBacktrace = backtrace(backtraceAddr, 64);
	backtraceSymbols = backtrace_symbols(backtraceAddr, numBacktrace);
	ss << "Backtrace: " << std::endl;
	for (int i = 0; i < numBacktrace; i++) {
		ss << backtraceSymbols[i] << std::endl;
	}
#elif WIN32
	void* backtraceAddr[64];
	int numBackTrace = CaptureStackBackTrace(1, 62, backtraceAddr, nullptr);
	ss << "Backtrace: " << std::endl;
	HANDLE currentProcess = ::GetCurrentProcess();
	if (!symbolsInitialized)
	{
		SymInitialize(currentProcess, nullptr, true);
	}
	for (int i = 0; i < numBackTrace; i++) {
		SYMBOL_INFO si;
		if (SymFromAddr(currentProcess, reinterpret_cast<DWORD64>(backtraceAddr[i]), 0, &si))
		{
			ss << "[0x" << backtraceAddr[i] << "] - " << si.Name << std::endl;
		}
		else
		{
			ss << "[0x" << backtraceAddr[i] << "] - Unresolved address" << std::endl;
		}
	}
#endif

    std::string what_str = ss.str();
    auto *what = new char[what_str.length()+1];

    ::strcpy(what, what_str.c_str());
    m_what = what;
}
