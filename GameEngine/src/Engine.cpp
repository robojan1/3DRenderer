#include <engine/engine.h>

using namespace rj;

Engine::Engine(std::chrono::milliseconds updateTimeStep) : _isRunning(false), _timePerUpdate(updateTimeStep)
{
	initialize();
}

Engine::~Engine()
{
	release();
}

void Engine::stop() {
	_isRunning = false;
}
