#include <engine/engine.h>
#define NOMINMAX
#include <Windows.h>
#include <ratio>

using namespace rj;

void Engine::run()
{
	using clock = std::chrono::high_resolution_clock;
	using duration = std::chrono::duration<float>;
	::MSG msg = {};

	_isRunning = true;

	auto lastUpdate = clock::now() - _timePerUpdate;
	auto lastRender = lastUpdate;
	while(_isRunning)
	{
		while (::PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
		while(clock::now() - _timePerUpdate > lastUpdate)
		{
			_updateInputCallbacks();
			_updateCallbacks(std::chrono::duration_cast<duration>(_timePerUpdate).count());
			_timerManager.processExpiredTimers();
			lastUpdate += _timePerUpdate;
		}
		auto now = clock::now();
		auto progressInUpdate = std::chrono::duration_cast<duration>(now - lastUpdate).count() /
			std::chrono::duration_cast<duration>(_timePerUpdate).count();
		auto timeDelta = std::chrono::duration_cast<duration>(now - lastRender).count();
		lastRender = now;
		_renderCallbacks(progressInUpdate, timeDelta);
	}
}

void Engine::initialize() {}

void Engine::release() {}
