
#include <engine/win32/WinGLWindow.h>
#include <engine/Exception.h>
#include <memory>
#include <engine/KeyBoard.h>

using namespace rj;

thread_local WinGLWindow* WinGLWindow::_newWindowPtr = nullptr;
std::unordered_map<::HWND, WinGLWindow*> WinGLWindow::_hwndCache;
std::mutex WinGLWindow::_hwndCacheMutex;


WinGLWindow::WinGLWindow(std::shared_ptr<Engine> engine, int width, int height, int screenWidth, int screenHeight) :
	BaseGLWindow(std::move(engine), width, height), _hwnd(nullptr), _hglrc(nullptr)
{
	CreateWinWindow(screenWidth, screenHeight);
	_renderCallbackHandle = _engine->addRenderCallback(std::bind(&WinGLWindow::render, this, std::placeholders::_1, std::placeholders::_2));
}

WinGLWindow::~WinGLWindow()
{
	DestroyWindow();
	_engine->removeRenderCallback(_renderCallbackHandle);
}

void WinGLWindow::SetName(const std::string& name)
{
	BaseGLWindow::SetName(name);
	::SetWindowTextA(_hwnd, name.c_str());
}

void WinGLWindow::UpdateScreen()
{
	auto hdc = ::GetDC(_hwnd);
	wglMakeCurrent(hdc, _hglrc);
	BaseGLWindow::UpdateScreen();
	::SwapBuffers(hdc);	
}

void WinGLWindow::CreateWinWindow(int width, int height)
{
	const char CLASS_NAME[] = "WinGLWindow Class";
	HINSTANCE hInstance = ::GetModuleHandle(nullptr);
	WNDCLASS wc = { };
	wc.lpfnWndProc = &WinGLWindow::WindowProc;
	wc.hInstance = hInstance;
	wc.lpszClassName = CLASS_NAME;
	wc.style = CS_OWNDC;

	RegisterClass(&wc);

	m_screenWidth = width;
	m_screenHeight = height;

	_newWindowPtr = this;
	_hwnd = ::CreateWindowEx(
		0,
		CLASS_NAME, 
		"WinGLWindow", 
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 
		CW_USEDEFAULT,
		width,
		height,
		nullptr,
		nullptr,
		hInstance,
		this);
	if(_hwnd == nullptr)
	{
		throw rj::Exception("Windows Exception", "Could not create the window");
	}

	_keyboard = std::make_unique<rj::KeyBoard>(_engine);

	ShowWindow(_hwnd, SW_SHOW);
}

void WinGLWindow::DestroyWindow()
{
	
}

LRESULT WinGLWindow::DoCreateWindow(HWND hwnd, LPCREATESTRUCT info)
{
	PIXELFORMATDESCRIPTOR pfd;
	ZeroMemory(&pfd, sizeof(pfd));
	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 24;
	pfd.cStencilBits = 8;
	pfd.cAuxBuffers = 0;
	pfd.iLayerType = PFD_MAIN_PLANE;

	HDC dc = GetDC(hwnd);

	int chosenPf = ::ChoosePixelFormat(dc, &pfd);
	if(chosenPf == 0)
	{
		MessageBox(0, "Could not find a suitable pixel descriptor", "Error", 0);
		throw rj::Exception("Windows Exception", "Could not find a suitable pixel descriptor");
	}

	SetPixelFormat(dc, chosenPf, &pfd);

	_hglrc = wglCreateContext(dc);
	wglMakeCurrent(dc, _hglrc);

	OnGLInit();
	return ERROR_SUCCESS;
}

void WinGLWindow::onKeyEvent(WPARAM wparam, LPARAM lparam, bool pressed)
{
	int keycode;
	switch (wparam)
	{
	case VK_RETURN:
		keycode = (lparam & (1 << 24)) != 0 ? 0x7 : VK_RETURN;
		break;
	case VK_CONTROL: {
		// We can get this on right alt key ...
		_keyboard->OnKeyEvent(VK_LCONTROL, (GetKeyState(VK_LCONTROL) & 0x8000) != 0);
		_keyboard->OnKeyEvent(VK_RCONTROL, (GetKeyState(VK_RCONTROL) & 0x8000) != 0);
		return;
	}
	case VK_SHIFT: {
		// Can't determine which shift key is pressed send both
		_keyboard->OnKeyEvent(VK_LSHIFT, (GetKeyState(VK_LSHIFT) & 0x8000) != 0);
		_keyboard->OnKeyEvent(VK_RSHIFT, (GetKeyState(VK_RSHIFT) & 0x8000) != 0);
		return;
	}
	case VK_MENU:
	{
		keycode = (lparam & (1 << 24)) != 0 ? VK_RMENU : VK_LMENU;
		break;
	}
	default:
		keycode = wparam;
		break;
	}
	printf("Key event: %d %s\n", keycode, pressed ? "pressed" : "released");
	_keyboard->OnKeyEvent(keycode, pressed);
}

void WinGLWindow::SetFPSLabel(float elapsedTime)
{
	BaseGLWindow::SetFPSLabel(elapsedTime);
	// extra len " - FPS: 00000" is 13
	size_t labelSize = Name().size() + 1 + 13;
	std::vector<char> label(labelSize);
	auto fps = static_cast<int>(1 / elapsedTime);
	snprintf(label.data(), labelSize, "%s - FPS: %d", Name().c_str(), fps);
	::SetWindowTextA(_hwnd, label.data());
}

void WinGLWindow::render(float updateProgress, float elapsedTime)
{
	(void)updateProgress;
	auto hdc = ::GetDC(_hwnd);
	wglMakeCurrent(hdc, _hglrc);
	OnGLRender(elapsedTime);
	::SwapBuffers(hdc);
}

LRESULT CALLBACK WinGLWindow::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	WinGLWindow* self;
	if(_newWindowPtr != nullptr)
	{
		std::lock_guard<std::mutex> lk(_hwndCacheMutex);
		_hwndCache[hwnd] = _newWindowPtr;
		self = _newWindowPtr;
		_newWindowPtr = nullptr;
	} else
	{
		std::lock_guard<std::mutex> lk(_hwndCacheMutex);
		self = _hwndCache.at(hwnd);
	}
	switch(uMsg)
	{
	case WM_DESTROY:
		::PostQuitMessage(0);
		return 0;
	case WM_PAINT: {
		::PAINTSTRUCT ps;
		::HDC  hdc = ::BeginPaint(hwnd, &ps);
		::FillRect(hdc, &ps.rcPaint, (::HBRUSH)(COLOR_WINDOW + 1));
		::EndPaint(hwnd, &ps);
		return 0;
	}
	case WM_CREATE: {
		return self->DoCreateWindow(hwnd, reinterpret_cast<LPCREATESTRUCT>(lParam));
	}
	case WM_KEYDOWN:{
		printf("Keydown %02x %d\n", wParam, (lParam >> 24) & 1);
		self->onKeyEvent(wParam, lParam, true);
		return 0;
	}
	case WM_KEYUP:{
		printf("Keyup %02x %d\n", wParam, (lParam >> 24) & 1);
		self->onKeyEvent(wParam, lParam, false);
		return 0;
	}
	case WM_SYSKEYDOWN:{
		printf("sys Keydown %02x %d\n", wParam, (lParam >> 24) & 1);
		self->onKeyEvent(wParam, lParam, true);
		return 0;
	}
	case WM_SYSKEYUP: {
		printf("sys Keyup %02x %d\n", wParam, (lParam >> 24) & 1);
		self->onKeyEvent(wParam, lParam, false);
		return 0;
	}
	default:
		return ::DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
}
