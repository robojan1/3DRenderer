#include <engine/TimerManager.h>
#include <chrono>
#define NOMINMAX
#include <Windows.h>

using namespace rj;

bool TimerManager::waitUntilNextTimer(std::chrono::milliseconds timeout) const
{
	std::chrono::nanoseconds waitTime;
	DWORD dwWaitTime;
	bool hasTimerExpired = true;
	do {
		auto waitTime = timeUntilExpiration();
		if (timeout < waitTime) {
			waitTime = timeout;
			hasTimerExpired = false;
		}
		dwWaitTime = static_cast<DWORD>(std::chrono::duration_cast<std::chrono::milliseconds>(waitTime).count());
	} while (::SleepEx(dwWaitTime, TRUE) == WAIT_IO_COMPLETION);
	return hasTimerExpired;
}
