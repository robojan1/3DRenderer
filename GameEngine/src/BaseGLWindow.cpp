//
// Created by Robbert-Jan de Jager on 15-9-18.
//

#include <engine/GLWindow.h>
#include <engine/InitException.h>
#include <engine/GLException.h>
#include <engine/MiscExceptions.h>
#include <engine/KeyBoard.h>
#include <stdexcept>
#include <chrono>
#include <GL/glew.h>
#include <vector>

using namespace rj;

BaseGLWindow::BaseGLWindow(std::shared_ptr<Engine> engine, int width, int height) :
	_engine(std::move(engine)), m_width(width), m_height(height), m_name("Window")
{
    m_framebuffer = new Color[width * height];
}

BaseGLWindow::~BaseGLWindow() {
    delete[] m_framebuffer;
    m_framebuffer = nullptr;
}

void BaseGLWindow::SetPixel(int x, int y, Color c) {
    if(x < 0 || y < 0 || x >= Width() || y >= Height())
        throw std::range_error("Position out of range");
    m_framebuffer[y*Width()+x] = c;
}

int BaseGLWindow::ScreenWidth() const {
    return m_screenWidth;
}

int BaseGLWindow::ScreenHeight() const {
    return m_screenHeight;
}

void BaseGLWindow::Tick(float timeElapsed) {
    OnGLRender(timeElapsed);
}

void BaseGLWindow::SetName(const std::string &name) {
    m_name = name;
}

void BaseGLWindow::OnGLRender(float elapsedTime) {
    SetFPSLabel(elapsedTime);

    OnRender(elapsedTime);

    DrawFrameBuffer();
}

void BaseGLWindow::OnRender(float elapsedTime) {
    (void)elapsedTime;
}

void BaseGLWindow::OnGLInit() {
    static const char *vertexShaderSource =
            "#version 130\n"
            "\n"
            "in vec2 vPos;\n"
            "out vec2 fUV;\n"
            "\n"
            "void main() {\n"
            "   gl_Position.xy = vPos;\n"
            "   gl_Position.zw = vec2(0.0, 1.0);\n"
            "   fUV = vec2(0.0, 1.0) + (vPos + vec2(1.0, 1.0)) * vec2(0.5, -0.5);\n"
            "}\n";
    static const char *fragmentShaderSource =
            "#version 130\n"
            "\n"
            "in vec2 fUV;\n"
            "out vec4 fragColor;\n"
            "uniform sampler2D uTex;\n"
            "\n"
            "void main() {\n"
            "   fragColor = texture(uTex, fUV);\n"
            "}\n";
    static const GLfloat vertices[] = {
            1.0f, 1.0f,
            1.0f, -1.0f,
            -1.0f, 1.0f,
            -1.0f, -1.0f
    };
    GLint result = GL_FALSE;
    int infoLength;

    result = glewInit();
    if(result != GLEW_OK) {
        throw rj::InitException("Could not initialize glew: %d\n", result);
    }

    // Create the VAO
    GL_CHECK(glGenVertexArrays(1, &m_vao));
    GL_CHECK(glBindVertexArray(m_vao));

    // Create the vertex buffer
    GL_CHECK(glGenBuffers(1, &m_vertexBuffer));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer));
    GL_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW));

    // Create the texture
    GL_CHECK(glGenTextures(1, &m_texture));
    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texture));
    GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Width(), Height(),
                          0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    GL_CHECK(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));

    // Create the shaders
    GL_CHECK(m_vertexShader = glCreateShader(GL_VERTEX_SHADER));
    GL_CHECK(m_fragmentShader = glCreateShader(GL_FRAGMENT_SHADER));
    GL_CHECK(glShaderSource(m_vertexShader, 1, &vertexShaderSource, nullptr));
    GL_CHECK(glCompileShader(m_vertexShader));

    glGetShaderiv(m_vertexShader, GL_COMPILE_STATUS, &result);
    glGetShaderiv(m_vertexShader, GL_INFO_LOG_LENGTH, &infoLength);
    if(infoLength > 0) {
        std::vector<char> info(static_cast<unsigned int>(infoLength + 1));
        glGetShaderInfoLog(m_vertexShader, infoLength, nullptr, info.data());
        throw rj::GLException("Error compiling vertex shader: %s", info.data());
    }

    GL_CHECK(glShaderSource(m_fragmentShader, 1, &fragmentShaderSource, nullptr));
    GL_CHECK(glCompileShader(m_fragmentShader));

    glGetShaderiv(m_fragmentShader, GL_COMPILE_STATUS, &result);
    glGetShaderiv(m_fragmentShader, GL_INFO_LOG_LENGTH, &infoLength);
    if(infoLength > 0) {
        std::vector<char> info(static_cast<unsigned int>(infoLength + 1));
        glGetShaderInfoLog(m_fragmentShader, infoLength, nullptr, info.data());
        throw rj::GLException("Error compiling fragment shader: %s", info.data());
    }

    GL_CHECK(m_shaderProgram = glCreateProgram());
    GL_CHECK(glAttachShader(m_shaderProgram, m_vertexShader));
    GL_CHECK(glAttachShader(m_shaderProgram, m_fragmentShader));
    GL_CHECK(glLinkProgram(m_shaderProgram));

    glGetProgramiv(m_shaderProgram, GL_LINK_STATUS, &result);
    glGetProgramiv(m_shaderProgram, GL_INFO_LOG_LENGTH, &infoLength);
    if(infoLength > 0) {
        std::vector<char> info(static_cast<unsigned int>(infoLength + 1));
        glGetProgramInfoLog(m_shaderProgram, infoLength, nullptr, info.data());
        throw rj::GLException("Error linking shader program: %s", info.data());
    }

    GL_CHECK(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
}

void BaseGLWindow::UpdateScreen() {
    DrawFrameBuffer();
}

void BaseGLWindow::DrawFrameBuffer() {
    GL_CHECK(glViewport(0, 0, ScreenWidth(), ScreenHeight()));

    GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texture));
    GL_CHECK(glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, Width(), Height(), GL_RGBA,
                             GL_UNSIGNED_BYTE, m_framebuffer));

    GL_CHECK(glClear(GL_COLOR_BUFFER_BIT));

    GL_CHECK(glUseProgram(m_shaderProgram));
    GL_CHECK(glEnableVertexAttribArray(0));
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer));
    GL_CHECK(glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr));
    GL_CHECK(glActiveTexture(GL_TEXTURE0));
    GL_CHECK(glUniform1i(0, 0));
    GL_CHECK(glDrawArrays(GL_TRIANGLE_STRIP, 0, 4));
    GL_CHECK(glDisableVertexAttribArray(0));
}

void BaseGLWindow::SetFPSLabel(float elapsedTime) {

}
