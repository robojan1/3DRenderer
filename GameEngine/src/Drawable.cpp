//
// Created by robojan on 15-9-18.
//

#include <utility>
#include <cstdlib>
#include <algorithm>
#include <engine/Drawable.h>

using namespace rj;

void Drawable::DrawLine(Point2i p1, Point2i p2, Color c)
{
    int x1 = p1[0];
    int y1 = p1[1];
    int x2 = p2[0];
    int y2 = p2[1];
    int dx = x2 - x1;
    int dy = y2 - y1;

    if(std::abs(dy) < std::abs(dx)) {
        // Make sure x1 is always smaller than x2
        if(x1 > x2) {
            int t = x1; x1 = x2; x2 = t;;
            y1 = y2;
            dx = -dx; dy = -dy;
        }
        int ystep = dy < 0 ? -1 : 1;
        if(dy < 0) dy = -dy;
        int error = 2 * dy - dx;
        int y = y1;

        for(int x = x1; x <= x2; x++)
        {
            SetPixel(x,y,c);
            if(error > 0) {
                y += ystep;
                error -= 2*dx;
            }
            error += 2*dy;
        }
    } else {
        // Make sure y1 is always smaller than y2
        if(y2 < y1) {
            int t = y1; y1 = y2; y2 = t;
            x1 = x2;
            dx = -dx; dy = -dy;
        }
        int xstep = dx < 0 ? -1 : 1;
        if(dx < 0) dx = -dx;
        int error = 2 * dx - dy;
        int x = x1;

        for(int y = y1; y <= y2; y++)
        {
            SetPixel(x, y, c);
            if(error > 0) {
                x += xstep;
                error -= 2*dy;
            }
            error += 2*dx;
        }
    }
}

void Drawable::DrawFilledRect(Point2i p, int width, int height, Color c) {
    int x = p[0];
    int y = p[1];
    for(int i = y; i < y + height; i++) {
        for(int j = x; j < x + width; j++) {
            SetPixel(j, i, c);
        }
    }
}

void Drawable::DrawRect(Point2i p, int width, int height, Color c) {
    int x = p[0];
    int y = p[1];
    for(int i = x; i < x + width; i++) {
        SetPixel(i, y, c);
    }
    for(int i = y + 1; i < y + height - 1; i++) {
        SetPixel(x, i, c);
        SetPixel(x + width - 1, i, c);
    }
    for(int i = x; i < x + width; i++) {
        SetPixel(i, y + height - 1, c);
    }
}

void Drawable::UpdateScreen() {
}

void Drawable::DrawFilledTriangle(Point2i p1, Point2i p2, Point2i p3, Color c) {
    int x1 = p1[0];
    int y1 = p1[1];
    int x2 = p2[0];
    int y2 = p2[1];
    int x3 = p3[0];
    int y3 = p3[1];
    // Sort the three points with y1 < y2 < y3
    if(y1 > y2) {
        std::swap(y1, y2);
        std::swap(x1, x2);
    }
    if(y2 > y3) {
        std::swap(y2, y3);
        std::swap(x2, x3);
    }
    if(y1 > y2) {
        std::swap(y1, y2);
        std::swap(x1, x2);
    }

    auto drawFlatTop = [this, c](int x1, int x2, int y1, int x3, int y3) {
        if(x2 < x1) {
            std::swap(x1, x2);
        }
        if(y3 == y1) {
            int xl = std::min(x1, x3);
            int xr = std::max(x2, x3);
            for(int x = xl; x <= xr; x++) {
                SetPixel(x, y1, c);
            }
        } else {
            int d1 = (x3 - x1) / (y3 - y1);
            int e1 = (x3 - x1) % (y3 - y1);
            int d2 = (x3 - x2) / (y3 - y1);
            int e2 = (x3 - x2) % (y3 - y1);
            int xl = x1;
            int xr = x2;
            int c1 = 0;
            int c2 = 0;
            for(int y = y1; y <= y3; y++) {
                for(int x = xl; x <= xr; x++) {
                    SetPixel(x, y, c);
                }
                c1 += e1;
                c2 += e2;
                xl += d1 + c1 / (y3 - y1);
                xr += d2 + c2 / (y3 - y1);
                if(std::abs(c1) >= y3 - y1) {
                    c1 %= y3 - y1;
                }
                if(std::abs(c2) >= y3 - y1) {
                    c2 %= y3 - y1;
                }
            }
        }
    };

    auto drawFlatBottom = [this, c](int x1, int y1, int x2, int x3, int y3) {
        if(x2 > x3) {
            std::swap(x2, x3);
        }
        if(y3 == y1) {
            int xl = std::min(x1, x2);
            int xr = std::max(x1, x3);
            for(int x = xl; x <= xr; x++) {
                SetPixel(x, y1, c);
            }
        } else {
            int d1 = (x2 - x1) / (y3 - y1);
            int e1 = (x2 - x1) % (y3 - y1);
            int d2 = (x3 - x1) / (y3 - y1);
            int e2 = (x3 - x1) % (y3 - y1);
            int xl = x1;
            int xr = x1;
            int c1 = 0;
            int c2 = 0;
            for(int y = y1; y <= y3; y++) {
                for(int x = xl; x <= xr; x++) {
                    SetPixel(x, y, c);
                }
                c1 += e1;
                c2 += e2;
                xl += d1 + c1 / (y3 - y1);
                xr += d2 + c2 / (y3 - y1);
                if(std::abs(c1) >= y3 - y1) {
                    c1 %= y3 - y1;
                }
                if(std::abs(c2) >= y3 - y1) {
                    c2 %= y3 - y1;
                }
            }
        }
    };

    if(y1 == y2) {
        // Flat top
        drawFlatTop(x1, x2, y1, x3, y3);
    } else if(y2 == y3) {
        // Flat bottom
        drawFlatBottom(x1, y1, x2, x3, y3);
    } else {
        // Find point of intersection
        int dx = x3 - x1;
        int dy = y3 - y1;
        int x4 = dy == 0 ? x1 : x1 + (dx * (y2 - y1)) / dy;

        drawFlatBottom(x1, y1, x2, x4, y2);
        drawFlatTop(x2, x4, y2, x3, y3);
    }
}

void Drawable::DrawTriangle(Point2i p1, Point2i p2, Point2i p3, Color c) {
    DrawLine(p1, p2, c);
    DrawLine(p1, p3, c);
    DrawLine(p2, p3, c);
}
