#include <engine/KeyBoard.h>

using namespace rj;

KeyBoard::KeyBoard(std::shared_ptr<Engine> engine) :
	_engine(std::move(engine)), _lastState{}, _currentState{}, _newState{}
{
	_inputCallbackHandle = _engine->addUpdateInputCallback(std::bind(&KeyBoard::SwitchFrame, this));
}

KeyBoard::~KeyBoard() {
	_engine->removeUpdateInputCallback(_inputCallbackHandle);
}

KeyBoard::KeyBoard(KeyBoard&& other) {
	_engine->removeUpdateInputCallback(other._inputCallbackHandle);
	_lastState = std::move(other._lastState);
	_currentState = std::move(other._currentState);
	_newState = std::move(other._newState);

	_inputCallbackHandle = _engine->addUpdateInputCallback(std::bind(&KeyBoard::SwitchFrame, this));
}

KeyBoard::KeyBoard(const KeyBoard& other)
{
	_engine->removeUpdateInputCallback(other._inputCallbackHandle);
	_lastState = other._lastState;
	_currentState = other._currentState;
	_newState = other._newState;

	_inputCallbackHandle = _engine->addUpdateInputCallback(std::bind(&KeyBoard::SwitchFrame, this));
}

void KeyBoard::OnKeyEvent(int keyCode, bool pressed) {
	_newState[keyCode] = pressed;
}

void KeyBoard::SwitchFrame() {
	_lastState = std::move(_currentState);
	_currentState = _newState;
}
