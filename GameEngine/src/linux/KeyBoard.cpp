//
// Created by Robbert-Jan de Jager on 2-2-19.
//

#include <engine/KeyBoard.h>
#include <engine/MiscExceptions.h>
#include <engine/InitException.h>

using namespace rj;

int KeyBoard::KeyToKeyCode(Key key) {
	KeySym symbol = KeyToKeySym(key);
	int keycode = XKeysymToKeycode(_engine->getDisplay(), symbol);
	if(keycode == 0) {
		throw KeyboardException("Could not find keycode for symbol %x", symbol);
	}
	return keycode;
}

KeySym KeyBoard::KeyToKeySym(Key key) {
	switch(key) {
		case Key::F1: return XK_F1;
		case Key::F2: return XK_F2;
		case Key::F3: return XK_F3;
		case Key::F4: return XK_F4;
		case Key::F5: return XK_F5;
		case Key::F6: return XK_F6;
		case Key::F7: return XK_F7;
		case Key::F8: return XK_F8;
		case Key::F9: return XK_F9;
		case Key::F10: return XK_F10;
		case Key::F11: return XK_F11;
		case Key::F12: return XK_F12;
		case Key::Grave: return XK_dead_grave;
		case Key::Minus: return XK_minus;
		case Key::Equal: return XK_equal;
		case Key::LBracket: return XK_bracketleft;
		case Key::RBracket: return XK_bracketright;
		case Key::BackSlash: return XK_backslash;
		case Key::Semicolon: return XK_semicolon;
		case Key::Acute: return XK_dead_acute;
		case Key::Comma: return XK_comma;
		case Key::Period: return XK_period;
		case Key::Slash: return XK_slash;
		case Key::One: return XK_1;
		case Key::Two: return XK_2;
		case Key::Three: return XK_3;
		case Key::Four: return XK_4;
		case Key::Five: return XK_5;
		case Key::Six: return XK_6;
		case Key::Seven: return XK_7;
		case Key::Eight: return XK_8;
		case Key::Nine: return XK_9;
		case Key::Zero: return XK_0;
		case Key::Backspace: return XK_BackSpace;
		case Key::ESC: return XK_Escape;
		case Key::Tab: return XK_Tab;
		case Key::CapsLock: return XK_Caps_Lock;
		case Key::Enter: return XK_Return;
		case Key::LShift: return XK_Shift_L;
		case Key::RShift: return XK_Shift_R;
		case Key::LCtrl: return XK_Control_L;
		case Key::RCtrl: return XK_Control_R;
		case Key::LAlt: return XK_Alt_L;
		case Key::RAlt: return XK_Alt_R;
		case Key::LSuper: return XK_Super_L;
		case Key::RSuper: return XK_Super_R;
		case Key::Menu: return XK_Menu;
		case Key::Space: return XK_space;
		case Key::PrintScreen: return XK_Print;
		case Key::ScrollLock: return XK_Scroll_Lock;
		case Key::Pause: return XK_Pause;
		case Key::Insert: return XK_Insert;
		case Key::Delete: return XK_Delete;
		case Key::Home: return XK_Home;
		case Key::End: return XK_End;
		case Key::PageUp: return XK_Page_Up;
		case Key::PageDown: return XK_Page_Down;
		case Key::Up: return XK_Up;
		case Key::Down: return XK_Down;
		case Key::Left: return XK_Left;
		case Key::Right: return XK_Right;
		case Key::NumLock: return XK_Num_Lock;
		case Key::NumDivide: return XK_KP_Divide;
		case Key::NumMultiply: return XK_KP_Multiply;
		case Key::NumMinus: return XK_KP_Subtract;
		case Key::NumPlus: return XK_KP_Add;
		case Key::NumEnter: return XK_KP_Enter;
		case Key::NumDecimal: return XK_KP_Decimal;
		case Key::Num0: return XK_KP_0;
		case Key::Num1: return XK_KP_1;
		case Key::Num2: return XK_KP_2;
		case Key::Num3: return XK_KP_3;
		case Key::Num4: return XK_KP_4;
		case Key::Num5: return XK_KP_5;
		case Key::Num6: return XK_KP_6;
		case Key::Num7: return XK_KP_7;
		case Key::Num8: return XK_KP_8;
		case Key::Num9: return XK_KP_9;
		case Key::A: return XK_a;
		case Key::B: return XK_b;
		case Key::C: return XK_c;
		case Key::D: return XK_d;
		case Key::E: return XK_e;
		case Key::F: return XK_f;
		case Key::G: return XK_g;
		case Key::H: return XK_h;
		case Key::I: return XK_i;
		case Key::J: return XK_j;
		case Key::K: return XK_k;
		case Key::L: return XK_l;
		case Key::M: return XK_m;
		case Key::N: return XK_n;
		case Key::O: return XK_o;
		case Key::P: return XK_p;
		case Key::Q: return XK_q;
		case Key::R: return XK_r;
		case Key::S: return XK_s;
		case Key::T: return XK_t;
		case Key::U: return XK_u;
		case Key::V: return XK_v;
		case Key::W: return XK_w;
		case Key::X: return XK_x;
		case Key::Y: return XK_y;
		case Key::Z: return XK_z;
		default:
			throw KeyboardException("Invalid key specified: %d", key);
	}
}
