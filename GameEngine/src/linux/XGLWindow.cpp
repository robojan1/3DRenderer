//
// Created by Robbert-Jan de Jager on 10-3-19.
//
#include <engine/linux/XGLWindow.h>
#include <engine/InitException.h>
#include <engine/KeyBoard.h>
#include <X11/XKBlib.h>

using namespace rj;

XGLWindow::XGLWindow(std::shared_ptr<Engine> engine, int width, int height, int screenWidth, int screenHeight) :
		BaseGLWindow(engine, width, height), m_window(None), m_wmDelete(None), m_glContext(nullptr)
{
	GLint result = glxewInit();
	if(result != GLEW_OK) {
		throw rj::InitException("Could not initialize glxew: %d\n", result);
	}
	CreateXWindow(screenWidth, screenHeight);

	OnGLInit();
}

void XGLWindow::CreateXWindow(int width, int height) {
	Window root;
	Colormap cmap;
	XVisualInfo *vi;
	XSetWindowAttributes windowAttributes;
	int dummy1, dummy2, dummy3;

	static GLint attr[] = {
			GLX_RGBA,
			GLX_DEPTH_SIZE, 24,
			GLX_DOUBLEBUFFER,
			None
	};

	auto display = _engine->getDisplay();

	root = DefaultRootWindow(display);
	if(root == None) {
		throw rj::InitException("Could not get root window");
	}

	if(!glXQueryExtension(display, &dummy1, &dummy2)) {
		throw rj::InitException("GLX is not supported");
	}

	vi = ::glXChooseVisual(display, 0, attr);
	if(vi == nullptr) {
		throw rj::InitException("Could not find appropriate visual");
	}

	cmap = ::XCreateColormap(display, root, vi->visual, AllocNone);
	if(cmap == None) {
		throw rj::InitException("Could not create a colormap");
	}

	windowAttributes.colormap = cmap;
	windowAttributes.event_mask = StructureNotifyMask;

	_clientMessageHandle = _engine->addEventCallback(ClientMessage, [this](XEvent *evt) {
		if(evt->xclient.data.l[0] == m_wmDelete) {
			_engine->stop();
		}
	});
	_configureNotifyHandle = _engine->addEventCallback(ConfigureNotify,
			std::bind(&XGLWindow::OnResize, this, std::placeholders::_1));

	m_window = XCreateWindow(display, root,
	                         0, 0, static_cast<unsigned int>(width), static_cast<unsigned int>(height),
	                         0, vi->depth, InputOutput, vi->visual,
	                         CWColormap | CWEventMask, &windowAttributes);

	_keyboard = std::make_unique<rj::KeyBoard>(_engine);
	EnableKeyboardEvents();

	XMapWindow(display, m_window);
	::XStoreName(display, m_window, Name().c_str());

	m_wmDelete = XInternAtom(display, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(display, m_window, &m_wmDelete, 1);

	if(!XkbQueryExtension(display, &dummy1, &dummy2, &dummy3, nullptr, nullptr))
	{
		throw rj::InitException("Could not initialize the X keyboard extension");
	}
	if(!XkbSetDetectableAutoRepeat(display, True, nullptr))
	{
		throw rj::InitException("Could not configure the X keyboard extension");
	}


	m_glContext = glXCreateContext(display, vi, nullptr, GL_TRUE);
	if(m_glContext == nullptr) {
		throw rj::InitException("Could not create the OpenGL context");
	}

	glXMakeCurrent(display, m_window, m_glContext);

	XWindowAttributes getWinAttr;
	XGetWindowAttributes(display, m_window, &getWinAttr);

	m_screenWidth = getWinAttr.width;
	m_screenHeight = getWinAttr.height;

	_renderHandle = _engine->addRenderCallback(std::bind(&XGLWindow::render, this, std::placeholders::_1, std::placeholders::_2));
}

void XGLWindow::DestroyXWindow() {
	auto display = _engine->getDisplay();

	_engine->removeEventCallback(ClientMessage, _clientMessageHandle);
	_engine->removeEventCallback(ConfigureNotify, _configureNotifyHandle);
	_engine->removeEventCallback(KeyPress, _keyPressHandle);
	_engine->removeEventCallback(KeyRelease, _keyReleaseHandle);
	_engine->removeRenderCallback(_renderHandle);

	_keyboard.reset();
	glXMakeCurrent(display, None, nullptr);
	glXDestroyContext(display, m_glContext);
	XDestroyWindow(display, m_window);
	XCloseDisplay(display);
}

void XGLWindow::render(float progress, float timeElapsed) {
	(void)progress;
	BaseGLWindow::Tick(timeElapsed);
	glXSwapBuffers(_engine->getDisplay(), m_window);
}

void XGLWindow::OnResize(XEvent *evt) {
	m_screenWidth = evt->xconfigure.width;
	m_screenHeight = evt->xconfigure.height;

}

void XGLWindow::onKeyEvent(XEvent *evt) {
	if(evt->xkey.window != m_window) return;
	_keyboard->OnKeyEvent(evt->xkey.keycode, evt->type != KeyRelease);
}

void XGLWindow::SetFPSLabel(float elapsedTime) {
	BaseGLWindow::SetFPSLabel(elapsedTime);
	// extra len " - FPS: 00000" is 13
	size_t labelSize = Name().size() + 1 + 13;
	char label[labelSize];
	auto fps = static_cast<int>(1 / elapsedTime);
	snprintf(label, labelSize, "%s - FPS: %d", Name().c_str(), fps);
	::XStoreName(_engine->getDisplay(), m_window, label);
}

void XGLWindow::UpdateScreen() {
	BaseGLWindow::UpdateScreen();

	glXSwapBuffers(_engine->getDisplay(), m_window);
}

void XGLWindow::SetName(const std::string &name) {
	BaseGLWindow::SetName(name);
	::XStoreName(_engine->getDisplay(), m_window, Name().c_str());
}

XGLWindow::~XGLWindow() {
	DestroyXWindow();
}

void XGLWindow::EnableKeyboardEvents()
{
	auto display = _engine->getDisplay();
	XWindowAttributes winAttr;
	XSetWindowAttributes setAttr;

	if (!XGetWindowAttributes(display, m_window, &winAttr)) {
		throw InitException("Could not get window attributes");
	}

	setAttr.event_mask = winAttr.your_event_mask | KeyPressMask | KeyReleaseMask;
	XChangeWindowAttributes(display, m_window, CWEventMask, &setAttr);

	_keyPressHandle = _engine->addEventCallback(KeyPress, std::bind(&XGLWindow::onKeyEvent, this, std::placeholders::_1));
	_keyReleaseHandle = _engine->addEventCallback(KeyRelease, std::bind(&XGLWindow::onKeyEvent, this, std::placeholders::_1));
}
