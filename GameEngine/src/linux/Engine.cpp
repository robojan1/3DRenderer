#include <engine/engine.h>
#include <engine/InitException.h>

using namespace rj;

void Engine::run()
{
	using clock = std::chrono::high_resolution_clock;
	using duration = std::chrono::duration<float>;

	XEvent evt;

	_isRunning = true;

	auto lastUpdate = clock::now() - _timePerUpdate;
	auto lastRender = lastUpdate;
	while(_isRunning)
	{
		// Handle inputs
		while(XPending(_display) > 0) {
			XNextEvent(_display, &evt);

			dispatchEvent(&evt);
		}
		while(clock::now() - _timePerUpdate > lastUpdate)
		{
			_updateInputCallbacks();
			_updateCallbacks(std::chrono::duration_cast<duration>(_timePerUpdate).count());
			_timerManager.processExpiredTimers();
			lastUpdate += _timePerUpdate;
		}
		auto now = clock::now();
		auto progressInUpdate = std::chrono::duration_cast<duration>(now - lastUpdate).count() /
		                        std::chrono::duration_cast<duration>(_timePerUpdate).count();
		auto timeDelta = std::chrono::duration_cast<duration>(now - lastRender).count();
		lastRender = now;
		_renderCallbacks(progressInUpdate, timeDelta);
	}
}

Callback<XEvent *>::Handle Engine::addEventCallback(int type, Callback<XEvent *> cb)
{
	return _eventCallbacks[type].add(cb);
}

void Engine::removeEventCallback(int type, Callback<XEvent *>::Handle handle)
{
	auto listIt = _eventCallbacks.find(type);
	if(listIt == _eventCallbacks.end()) return;

	listIt->second.remove(handle);
}

Display *Engine::getDisplay()
{
	return _display;
}

void Engine::initialize() {
	_display = XOpenDisplay(nullptr);
	if(_display == nullptr) {
		throw rj::InitException("Could not open Display");
	}
}

void Engine::release() {
	XCloseDisplay(_display);
}

void Engine::dispatchEvent(XEvent *event)
{
	int type = event->type;
	auto listIt = _eventCallbacks.find(type);
	if(listIt == _eventCallbacks.end()) return;

	listIt->second(event);
}
