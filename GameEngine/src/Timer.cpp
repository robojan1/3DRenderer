#include <engine/Timer.h>

rj::Timer::Timer(time_point t) : 
	_expirationTime(t), _looping(false), _period(std::chrono::nanoseconds::zero())
{
}

rj::Timer::Timer(std::chrono::nanoseconds period, bool looping) : 
	_expirationTime(clock::now() + period), _looping(looping), _period(period)
{
}

rj::Timer::Timer(time_point start_time, std::chrono::nanoseconds period, bool looping) : 
	_expirationTime(start_time), _looping(looping), _period(period)
{
}

void rj::Timer::process()
{
	if(timeoutCallback)
	{
		timeoutCallback();
	}
	if(isLooping())
	{
		_expirationTime = _expirationTime + _period;
	}
}

std::chrono::nanoseconds rj::Timer::getTimeUntilExpiration() const
{
	return std::chrono::duration_cast<std::chrono::nanoseconds>(_expirationTime - clock::now());
}
