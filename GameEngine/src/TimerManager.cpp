#include <engine/TimerManager.h>

using namespace rj;

TimerManager::TimerManager()
{
	
}

TimerManager::~TimerManager()
{
	
}

std::chrono::nanoseconds TimerManager::timeUntilExpiration() const
{
	while(!_queue.empty())
	{
		auto timer = _queue.top().lock();
		if(timer)
		{
			return timer->getTimeUntilExpiration();
		} else
		{
			_queue.pop();
		}
	}
	return std::chrono::nanoseconds::max();
}

void TimerManager::processExpiredTimers()
{
	while (!_queue.empty())
	{
		auto timer = _queue.top().lock();
		if (timer)
		{
			if(timer->getTimeUntilExpiration() > std::chrono::nanoseconds::zero())
			{
				return; // Not yet expired
			}
			timer->process();
			_queue.pop();
			if(timer->getTimeUntilExpiration() > std::chrono::nanoseconds::zero())
			{
				addTimer(timer);
			}
		}
		else
		{
			_queue.pop();
		}
	}
}

void TimerManager::addTimer(std::weak_ptr<Timer> timer)
{
	_queue.emplace(std::move(timer));
}

bool TimerManager::queueComparitor::operator()(const std::weak_ptr<Timer>& lhs, const std::weak_ptr<Timer>& rhs)
{
	auto lhs_obj = lhs.lock();
	auto rhs_obj = rhs.lock();
	if(!lhs_obj || !rhs_obj)
	{
		return static_cast<bool>(rhs_obj);
	}
	return *lhs_obj < *rhs_obj;
}
