//
// Created by Robbert-Jan de Jager on 2-2-19.
//

#ifndef INC_3DRENDERER_KEYBOARD_H
#define INC_3DRENDERER_KEYBOARD_H

#include <engine/Keys.h>
#include <array>
#include <vector>
#include "engine.h"
#ifdef __linux__
#include <X11/keysym.h>
#include <X11/Xlib.h>
#endif

namespace rj {

class KeyBoard {
public:
	explicit KeyBoard(std::shared_ptr<Engine> engine);
	explicit KeyBoard(KeyBoard&& other);
	explicit KeyBoard(const KeyBoard& other);
	virtual ~KeyBoard();

#ifdef __linux__
	static KeySym KeyToKeySym(Key key);
#endif
	virtual int KeyToKeyCode(Key key);

	inline bool IsPressed(Key key) { return IsPressed(KeyToKeyCode(key)); }
	inline bool IsReleased(Key key) { return IsReleased(KeyToKeyCode(key)); }
	inline bool IsReleasing(Key key) { return IsReleasing(KeyToKeyCode(key)); }
	inline bool IsPressing(Key key) { return IsPressing(KeyToKeyCode(key)); }
	inline bool IsChanging(Key key) { return IsChanging(KeyToKeyCode(key)); }

	inline bool IsPressed(int keyCode) { return _currentState[keyCode]; }
	inline bool IsReleased(int keyCode) { return !_currentState[keyCode]; }
	inline bool IsReleasing(int keyCode) { return IsReleased(keyCode) && _lastState[keyCode]; }
	inline bool IsPressing(int keyCode) { return IsPressed(keyCode) && !_lastState[keyCode]; }
	inline bool IsChanging(int keyCode) { return _currentState[keyCode] != _lastState[keyCode]; }

	void SwitchFrame();
	void OnKeyEvent(int keyCode, bool pressed);
private:

#ifdef __linux__
	void OnXKeyEvent(XEvent* event);
#endif

	static constexpr int NumKeyCodes = 256;

	std::array<bool, NumKeyCodes> _lastState;
	std::array<bool, NumKeyCodes> _currentState;
	std::array<bool, NumKeyCodes> _newState;
	Callback<>::Handle _inputCallbackHandle;
	std::shared_ptr<Engine> _engine;

};
}

#endif //INC_3DRENDERER_KEYBOARD_H
