#pragma once

#include <functional>
#include <atomic>
#include <vector>
#include <algorithm>

namespace rj
{
	template<class... Args>
	class Callback
	{
	public:
		using Handle = int;
		static constexpr Handle InvalidHandle = -1;

		template <class T>
		Callback(T callback) :
			callback(std::move(callback)), _handle(_handleCounter++)
		{

		}

		Handle handle() const { return _handle; }

		void operator()(Args... args) const
		{
			if(callback)
			{
				callback(args...);
			}
		}

		bool operator==(Handle h) const
		{
			return _handle == h;
		}

		std::function<void(Args...)> callback;
	private:
		static std::atomic<Handle> _handleCounter;
		Handle _handle;
	};

	template<class... Args>
	std::atomic<typename Callback<Args...>::Handle> Callback<Args...>::_handleCounter = 0;

	template<class... Args>
	class CallbackList
	{
	public:
		CallbackList() = default;
		~CallbackList() = default;

		typename Callback<Args...>::Handle add(Callback<Args...> cb)
		{
			auto h = cb.handle();
			_callbacks.emplace_back(std::move(cb));
			return h;
		}
		bool remove(typename Callback<Args...>::Handle handle)
		{
			auto it = std::find(_callbacks.begin(), _callbacks.end(), handle);
			if(it != _callbacks.end())
			{
				_callbacks.erase(it);
				return true;
			}
			return false;
		}

		void operator() (Args... args) const
		{
			for(auto &callback : _callbacks)
			{
				callback(args...);
			}
		}

	private:
		std::vector<Callback<Args...>> _callbacks;

	};
}