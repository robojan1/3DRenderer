//
// Created by Robbert-Jan de Jager on 15-9-18.
//

#ifndef INC_3DRENDERER_EXCEPTION_H
#define INC_3DRENDERER_EXCEPTION_H

#include <stdexcept>

namespace rj {

    class Exception : public std::runtime_error {
    public:
        explicit Exception(const char *exceptionName, const char *format, ...);
        explicit Exception(const char *exceptionName, const char *format, va_list args);

        ~Exception() override;

        const char *what() const noexcept override;

    protected:
        Exception();

        void SetWhat(const char *exceptionName, const char *format, va_list args);

    private:
        const char *m_what;
    };
}


#endif //INC_3DRENDERER_EXCEPTION_H
