#pragma once

#include <functional>
#include <chrono>

namespace rj
{
	class Timer
	{
	public:
		using clock = std::chrono::high_resolution_clock;
		using time_point = std::chrono::time_point<clock>;
		explicit Timer(time_point t);
		Timer(std::chrono::nanoseconds period, bool looping);
		Timer(time_point start_time, std::chrono::nanoseconds period, bool looping);
		~Timer();

		void process();

		bool operator<(const Timer& rhs) const
		{
			return getExpirationTime() < rhs.getExpirationTime();
		}

		bool operator==(const Timer& rhs) const
		{
			return getExpirationTime() == rhs.getExpirationTime();
		}

		std::chrono::time_point<clock> getExpirationTime() const { return _expirationTime; }
		std::chrono::nanoseconds getTimeUntilExpiration() const;

		std::chrono::nanoseconds period() const { return _period; }
		bool isLooping() const { return _looping; }

		void setPeriod(std::chrono::nanoseconds period) { _period = period; }
		void setLooping(bool looping) { _looping = looping; }

		std::function<void(void)> timeoutCallback;
	private:
		std::chrono::time_point<clock> _expirationTime;
		bool _looping;
		std::chrono::nanoseconds _period;
	};
}