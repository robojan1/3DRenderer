//
// Created by Robbert-Jan de Jager on 17-3-19.
//

#ifndef TESTGAMES_TYPES_H
#define TESTGAMES_TYPES_H

#include <eigen3/Eigen/Eigen>

namespace rj {
	using Point2i = Eigen::Vector2i;
	using Point2f = Eigen::Vector2f;
	using Point3f = Eigen::Vector3f;
}

#endif //TESTGAMES_TYPES_H
