//
// Created by Robbert-Jan de Jager on 15-9-18.
//

#ifndef INC_3DRENDERER_INITEXCEPTION_H
#define INC_3DRENDERER_INITEXCEPTION_H

#include "Exception.h"
#include <cstdarg>

namespace rj {
    class InitException : public Exception {
    public:
        explicit InitException(const char *format, ...) : Exception() {
            va_list args;
            va_start(args, format);
            SetWhat("InitException", format, args);
            va_end(args);
        }
    private:
    };
}


#endif //INC_3DRENDERER_INITEXCEPTION_H
