//
// Created by Robbert-Jan de Jager on 2-2-19.
//

#ifndef INC_3DRENDERER_MISCEXCEPTIONS_H
#define INC_3DRENDERER_MISCEXCEPTIONS_H

#include "Exception.h"
#include <cstdarg>

namespace rj {
	class KeyboardException : public Exception {
	public:
		explicit KeyboardException(const char *format, ...) : Exception() {
			va_list args;
			va_start(args, format);
			SetWhat(__func__, format, args);
			va_end(args);
		}
	};
}

#endif //INC_3DRENDERER_MISCEXCEPTIONS_H
