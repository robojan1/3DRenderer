#pragma once

#include <chrono>
#include <queue>
#include <memory>
#include <engine/Timer.h>

namespace rj
{
	class TimerManager
	{
	public:
		TimerManager();
		~TimerManager();

		std::chrono::nanoseconds timeUntilExpiration() const;

		void processExpiredTimers();

		void addTimer(std::weak_ptr<Timer> timer);

		bool waitUntilNextTimer(std::chrono::milliseconds timeout = std::chrono::milliseconds::max()) const;

	private:
		struct queueComparitor {
			bool operator()(const std::weak_ptr<Timer>& lhs, const std::weak_ptr<Timer>& rhs);
		};
		mutable std::priority_queue<std::weak_ptr<Timer>, std::vector<std::weak_ptr<Timer>>, queueComparitor> _queue;
	};
}
