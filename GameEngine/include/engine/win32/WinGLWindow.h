//
// Created by Robbert-Jan de Jager on 10-3-19.
//

#ifndef TESTGAMES_WINGLWINDOW_H
#define TESTGAMES_WINGLWINDOW_H

#define NOMINMAX
#include <engine/GLWindow.h>
#include <engine/engine.h>
#include <Windows.h>
#include <mutex>
#include <unordered_map>

namespace rj {
class WinGLWindow: public BaseGLWindow
{
public:
	WinGLWindow(std::shared_ptr<Engine> engine, int width, int height, int screenWidth, int screenHeight);
	~WinGLWindow() override;

	void SetName(const std::string &name) override;

	void UpdateScreen() override;



private:
	::HWND _hwnd;
	::HGLRC _hglrc;
	Callback<float, float>::Handle _renderCallbackHandle;
	static thread_local WinGLWindow* _newWindowPtr;
	static std::unordered_map<::HWND, WinGLWindow*> _hwndCache;
	static std::mutex _hwndCacheMutex;

	void CreateWinWindow(int width, int height);
	void DestroyWindow();
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT DoCreateWindow(HWND hwnd, LPCREATESTRUCT info);

	void onKeyEvent(WPARAM wparam, LPARAM lparam, bool pressed);
	static int remapKeyCodes(WPARAM wparam, LPARAM lparam);

protected:
	void SetFPSLabel(float elapsedTime) override;

	void render(float updateProgress, float elapsedTime);

};
}

#endif //TESTGAMES_XGLWINDOW_H
