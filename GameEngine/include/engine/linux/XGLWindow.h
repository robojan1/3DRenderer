//
// Created by Robbert-Jan de Jager on 10-3-19.
//

#ifndef TESTGAMES_XGLWINDOW_H
#define TESTGAMES_XGLWINDOW_H

#include <engine/GLWindow.h>
#include <X11/Xlib.h>
#include <GL/glxew.h>

namespace rj {
class XGLWindow: public BaseGLWindow
{
public:
	XGLWindow(std::shared_ptr<Engine> engine, int width, int height, int screenWidth, int screenHeight);
	~XGLWindow() override;

	void SetName(const std::string &name) override;

	void UpdateScreen() override;

private:
	::Window m_window;
	::Atom m_wmDelete;
	GLXContext m_glContext;
	Callback<XEvent *>::Handle _clientMessageHandle;
	Callback<XEvent *>::Handle _configureNotifyHandle;
	Callback<XEvent *>::Handle _keyReleaseHandle;
	Callback<XEvent *>::Handle _keyPressHandle;
	Callback<float,float>::Handle _renderHandle;

	void CreateXWindow(int width, int height);
	void DestroyXWindow();

	void render(float progress, float timeElapsed);
	void OnResize(XEvent *evt);
	void onKeyEvent(XEvent *evt);

	void EnableKeyboardEvents();

protected:
	void SetFPSLabel(float elapsedTime) override;

};
}

#endif //TESTGAMES_XGLWINDOW_H
