//
// Created by robojan on 15-9-18.
//

#ifndef INC_3DRENDERER_DRAWABLE_H
#define INC_3DRENDERER_DRAWABLE_H

#include "Color.h"
#include <engine/types.h>

namespace rj {

    class Drawable {
    public:
        virtual ~Drawable() = default;
        virtual void SetPixel(int x, int y, Color c) = 0;
        virtual int Width() const = 0;
        virtual int Height() const = 0;
        virtual void UpdateScreen();
        virtual void DrawLine(Point2i p1, Point2i p2, Color c);
        virtual void DrawRect(Point2i p, int width, int height, Color c);
        virtual void DrawFilledRect(Point2i p, int width, int height, Color c);
        virtual void DrawTriangle(Point2i p1, Point2i p2, Point2i p3, Color c);
        virtual void DrawFilledTriangle(Point2i p1, Point2i p2, Point2i p3, Color c);
        template <typename C>
        void DrawPolygon(const C &container, Color c) {
            auto &it = container.begin();
            if(it == container.end()) return;
            auto &firstVal = *it;
            auto &lastVal = *it;
            it++;
            for(; it != container.end(); it++) {
                DrawLine(lastVal, *it, c);
                lastVal = *it;
            }
            DrawLine(lastVal, firstVal, c);
        }
        template <typename C>
        void DrawFilledPolygon(const C &container, Color c) {
            
        }
    private:

    };
}


#endif //INC_3DRENDERER_DRAWABLE_H
