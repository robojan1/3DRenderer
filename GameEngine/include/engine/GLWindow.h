//
// Created by Robbert-Jan de Jager on 15-9-18.
//

#ifndef INC_3DRENDERER_GLWINDOW_H
#define INC_3DRENDERER_GLWINDOW_H

#include "Drawable.h"
#include "KeyBoard.h"
#include <string>
#include <GL/glew.h>
#include <memory>
#include <vector>
#include <engine/engine.h>

namespace rj {
#ifdef __linux__
	class XGLWindow;
	using GLWindow = XGLWindow;
#elif WIN32
	class WinGLWindow;
	using GLWindow = WinGLWindow;
#endif


    class BaseGLWindow : public Drawable {
    public:
        BaseGLWindow(std::shared_ptr<Engine> engine, int width, int height);
        ~BaseGLWindow() override;

        void SetPixel(int x, int y, Color c) override;
        int Width() const override { return m_width; };
        int Height() const override { return m_height; };
        virtual int ScreenWidth() const;
        virtual int ScreenHeight() const;

        void UpdateScreen() override;
        virtual void DrawFrameBuffer();

        std::string Name() const { return m_name; }
        virtual void SetName(const std::string &name);

        inline rj::KeyBoard &KeyBoard() { return *_keyboard; };

    private:
        int m_width, m_height;

        // GL state
        GLuint m_vao;
        GLuint m_vertexBuffer;
        GLuint m_vertexShader;
        GLuint m_fragmentShader;
        GLuint m_shaderProgram;
        GLuint m_texture;

    protected:
		std::shared_ptr<Engine> _engine;
        std::string m_name;
	    int m_screenWidth, m_screenHeight;
	    Color *m_framebuffer;
	    std::unique_ptr<rj::KeyBoard> _keyboard;

        virtual void OnRender(float elapsedTime);
        virtual void OnGLRender(float elapsedTime);
        virtual void OnGLInit();

	    virtual void Tick(float timeElapsed);
	    virtual void SetFPSLabel(float elapsedTime);
    };
}

#ifdef __linux__
#include <engine/linux/XGLWindow.h>
#elif WIN32
#include <engine/win32/WinGLWindow.h>
#endif

#endif //INC_3DRENDERER_GLWINDOW_H
