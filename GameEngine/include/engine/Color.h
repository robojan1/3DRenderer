//
// Created by Robbert-Jan de Jager on 15-9-18.
//

#ifndef INC_3DRENDERER_COLOR_H
#define INC_3DRENDERER_COLOR_H

#include <cstdint>

namespace rj {

    class Color {
    public:
        union {
            uint32_t rgba;
            struct {
                uint8_t r;
                uint8_t g;
                uint8_t b;
                uint8_t a;
            };
        };
#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-pro-type-member-init"
        constexpr Color() noexcept : rgba(0xff000000) { }
        constexpr Color(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha = 255) noexcept:
                r(red), g(green), b(blue), a(alpha) { }
        constexpr Color(const Color &other) : rgba(other.rgba) {}
        constexpr explicit Color(uint32_t rgba) noexcept: rgba(rgba) { }
        constexpr Color(float red, float green, float blue, float alpha = 1.0f) noexcept:
                r(static_cast<uint8_t>(red < 0 ? 0 : red > 1 ? 255 : red * 255)),
                g(static_cast<uint8_t>(green < 0 ? 0u : green > 1 ? 255u : green*255)),
                b(static_cast<uint8_t>(blue < 0 ? 0u : blue > 1 ? 255u : blue*255)),
                a(static_cast<uint8_t>(alpha < 0 ? 0u : alpha > 1 ? 255u : alpha*255)) { }
#pragma clang diagnostic pop
        float RedF() const noexcept { return r / 255.0f; }
        float GreenF() const noexcept { return g / 255.0f; }
        float BlueF() const noexcept { return b / 255.0f; }
        float AlphaF() const noexcept { return a / 255.0f; }

    };

    namespace color {
        static constexpr Color BLACK = Color(0xFF000000);
        static constexpr Color WHITE = Color(0xFFFFFFFF);
        static constexpr Color SILVER = Color(0xFFC0C0C0);
        static constexpr Color GRAY = Color(0xFF808080);
        static constexpr Color RED = Color(0xFF0000FF);
        static constexpr Color MAROON = Color(0xFF000080);
        static constexpr Color GREEN = Color(0xFF008000);
        static constexpr Color LIME = Color(0xFF00FF00);
        static constexpr Color YELLOW = Color(0xFF00FFFF);
        static constexpr Color OLIVE = Color(0xFF008080);
        static constexpr Color BLUE = Color(0xFFFF0000);
        static constexpr Color NAVY = Color(0xFF800000);
        static constexpr Color PURPLE = Color(0xFF800080);
        static constexpr Color FUCHSIA = Color(0xFFFF00FF);
        static constexpr Color TEAL = Color(0xFF808000);
        static constexpr Color AQUA = Color(0xFFFFFF00);
    }
}


#endif //INC_3DRENDERER_COLOR_H
