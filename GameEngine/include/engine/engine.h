#pragma once

#include <engine/TimerManager.h>
#include <engine/CallbackList.h>
#include <chrono>

#ifdef __linux__
#include <X11/Xlib.h>
#include <unordered_map>
#endif

namespace rj
{
	class Engine
	{
	public:
		explicit Engine(std::chrono::milliseconds updateTimeStep = std::chrono::milliseconds(16));
		~Engine();

		void run();
		void stop();

		TimerManager& timerManager() { return _timerManager; }

		Callback<>::Handle addUpdateInputCallback(Callback<> cb) { return _updateInputCallbacks.add(std::move(cb)); }
		void removeUpdateInputCallback(Callback<>::Handle handle) { _updateInputCallbacks.remove(handle); }
		Callback<float>::Handle addUpdateCallback(Callback<float> cb) { return _updateCallbacks.add(std::move(cb)); }
		void removeUpdateCallback(Callback<float>::Handle handle) { _updateCallbacks.remove(handle); }
		Callback<float>::Handle addRenderCallback(Callback<float,float> cb) { return _renderCallbacks.add(std::move(cb)); }
		void removeRenderCallback(Callback<float,float>::Handle handle) { _renderCallbacks.remove(handle); }

#ifdef __linux__
		Callback<XEvent *>::Handle addEventCallback(int type, Callback<XEvent *> cb);
		void removeEventCallback(int type, Callback<XEvent *>::Handle handle);

		Display *getDisplay();
#endif

	private:
		bool _isRunning;
		TimerManager _timerManager;
		CallbackList<> _updateInputCallbacks;
		CallbackList<float> _updateCallbacks;
		CallbackList<float,float> _renderCallbacks;
		std::chrono::milliseconds _timePerUpdate;
#if defined(__linux__)
		std::unordered_map<int, CallbackList<XEvent *>> _eventCallbacks;
		Display *_display;

		void dispatchEvent(XEvent *event);
#elif defined(WIN32)
#endif

		void initialize();
		void release();
	};
}

