//
// Created by Robbert-Jan de Jager on 16-9-18.
//

#ifndef INC_3DRENDERER_GLEXCEPTION_H
#define INC_3DRENDERER_GLEXCEPTION_H

#include <cstdarg>
#include "Exception.h"
#include <GL/glew.h>
#include <GL/glu.h>

namespace rj {

    class GLException : public Exception {
    public:
        explicit GLException(const char *format, ...) : Exception() {
            va_list args;
            va_start(args, format);
            SetWhat("GLException", format, args);
            va_end(args);
        }
    };
#ifndef NDEBUG
#define GL_CHECK(x) \
    do {\
        GLenum status;\
        x;\
        status = glGetError();\
        if(status != GL_NO_ERROR) {\
            throw GLException("OpenGL exception (%d)%s", status, gluErrorString(status));\
        }\
    } while(0)
#else
#define GL_CHECK(x) x
#endif

#define GL_CHECK_ALWAYS(x) \
    do {\
        GLenum status;\
        x;\
        status = glGetError();\
        if(status != GL_NO_ERROR) {\
            throw GLException("OpenGL exception (%d)%s", status, gluErrorString(status));\
        }\
    } while(0)
}

#endif //INC_3DRENDERER_GLEXCEPTION_H
